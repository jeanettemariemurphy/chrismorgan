'use strict';

libraryApp.controller('LibraryController',
    function LibraryController($scope, libraryItemsData) {

        $scope.format = 'CD';
        console.log('log',libraryItemsData.getLibraryItems().length);

       if(libraryItemsData.getLibraryItems().length === 0) {
            libraryItemsData.setLibraryItems().then(
                function(items) { 
                    $scope.items = items; 
                    $scope.basketItems = 0;
                    console.log('typeof data', typeof items);
                },
                function(statusCode) { console.log(statusCode);}
            );
       } else {
            $scope.items = libraryItemsData.getLibraryItems();
            $scope.basketItems = libraryItemsData.getbasketCount();
            console.log('already exists', $scope.items);
       }


         $scope.loadFormat = function(format) {
            $scope.format = format;
         };

        $scope.addToBasket = function(item) {
        console.log('item.status', item.status);
           if(item.status === 'available') {
            item.status = 'selected';
           } else {
            item.status = 'available';
           }
           updateBasket(item.status);
          // updateItems();

           console.log('item.status after update', item.status);
           console.log($scope.items);


        };

        $scope.clearFilter = function() {
          $scope.query = "";
        };

        function updateBasket(action) {
            if(action === 'selected') {
                console.log('adding');
                $scope.basketItems++;
                libraryItemsData.updateLibraryItems($scope.items);
                libraryItemsData.updateBasketCount($scope.basketItems);

            } else {
                console.log('subtracting');
                $scope.basketItems--;
                libraryItemsData.updateLibraryItems($scope.items);
                libraryItemsData.updateBasketCount($scope.basketItems);
            }
        };


       /* function updateItems() {

            $scope.$watchCollection('items', function(newItems, oldItems) {
                console.log('updating scope', newItems, oldItems);
                $scope.items = newItems;

                 console.log('new items', $scope.items)
            });


        } */

       
    }
);