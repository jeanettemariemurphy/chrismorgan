'use strict';

libraryApp.controller('BasketController',
    function BasketController($scope, libraryItemsData, userData) {

       if(libraryItemsData.getLibraryItems().length === 0) {
            $scope.items = [];
       } else {
            $scope.items = libraryItemsData.getLibraryItems();
            $scope.basketItems = libraryItemsData.getbasketCount();
       }

       $scope.removeFromBasket = function(item) {
            item.status = "available";
            $scope.basketItems--;
            libraryItemsData.updateBasketCount($scope.basketItems);
       };

       $scope.validateForm = function(user, checkoutForm, results) {

          if(checkoutForm.$valid) {
              $scope.validSubmit = true;
              $scope.basketItems = 0;
              libraryItemsData.updateBasketCount($scope.basketItems);
              $scope.user = angular.copy(user);
              $scope.user.results = results;
              userData.setUser($scope.user);

              updateLibraryData();
          } else {
            $scope.submitted = true;

          }
      }

      function updateLibraryData() {
           $scope.items = $scope.items.filter(function(item) {
              if(item.status === 'selected') {
                item.status = "reserved";
              }
              return $scope.items;            
            });
            console.log('updated data', $scope.items);
            libraryItemsData.setUpdatedLibraryItems($scope.items);
            document.location.href = "/#/thankyou";
      }


       

       
    }
);