// example of working ajax json file overwrite

var obj = [

	{
		"composer": "MozartAAAA",
		"artist": "Dennis BrainAAAA",
		"title": "Horn Concertos"
	},

	{
		"composer": "HaydnBBBB",
		"artist": "Nikolas HarnoncortBBB",
		"title": "Paris Symonphonies"
	},

	{
		"composer": "MozartCCCC",
		"artist": "Neville MarrinerCCCc",
		"title": "Early Symphonies"
	}
];

console.log('obj', obj);
console.log('stringify', JSON.stringify(obj));

$.ajax({
    global: false,
    type: "POST",
    cache: false,
    dataType: "json",
    data: JSON.stringify(obj),
    contentType: "application/json",
    url: 'http://meggaweb.co.uk/chrismorgan/basket.php'
}).done(function(msg) {
  console.log( 'done', msg, obj );
}).fail(function(){
	console.log('doh', obj);
});