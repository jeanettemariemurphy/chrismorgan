libraryApp.factory('localStorageData', function() {
  return {
    setLibraryItems: function(items) { 
      localStorage.setItem('libraryItems', JSON.stringify(items));
      console.log('setting items');
    },

    getLibraryItems: function() {
      var libraryItems = localStorage.getItem('libraryItems');
      return JSON.parse(libraryItems);
    },

     removelibraryItems: function() { 
      localStorage.removeItem('libraryItems');
    },
    setBasketCount: function(count) { 
      localStorage.setItem('basketCount', count);
      console.log('setting basketCount');
    },

    getBasketCount: function() {
      var basketCount = localStorage.getItem('basketCount');
      return basketCount;
    },

     removeBasketCount: function() { 
      localStorage.removeItem('basketCount');
    }

  };


});