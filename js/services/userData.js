libraryApp.factory('userData', function($http, $q) {

  function user(firstname, lastname, email, basket){
      this.firstname = firstname;
      this.lastname = lastname;
      this.email = email;
      this.basket = basket;
  }

  function postUserData(obj) {
      $http({ 
          url: '/email.php', 
          method: 'POST', 
          data: obj, 
          headers: {
            'Content-Type': 'application/json'
          }
        }).
        success(function(data, status, headers, config) {
              console.log('success');
         }).
         error(function(data, status, headers, config) {
            console.log('fail');
         });


        
            }

  return {



    setUser: function(userData) {
        var newUser = new user(userData.firstname,userData.lastname,userData.email,userData.results);
        postUserData(JSON.stringify(newUser));
        console.log('stringified', JSON.stringify(newUser));
    }

  };

});