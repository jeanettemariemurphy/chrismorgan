libraryApp.factory('libraryItemsData', function($http, $q) {

  var libraryItems = [],
      basketCount = 0;

  return {
    setLibraryItems: function() {
      var deferred = $q.defer();
      $http({ method: 'GET', url: '../data/libraryItems.json'}).
        success(function(data, status, headers, config) {
          
              libraryItems = data;
              console.log('libraryItems are new', libraryItems, status);
              deferred.resolve(data);
         }).
         error(function(data, status, headers, config) {
             deferred.reject(status);
         });
        return deferred.promise;
    },

    getLibraryItems: function() {;
      return libraryItems;
    },

    updateLibraryItems: function(items) {
      libraryItems = items;
    },

    setUpdatedLibraryItems: function(items){
        var updatedItems = angular.toJson(items, true);

        $http({ 
          url: '/basket.php', 
          method: 'POST', 
          data: updatedItems, 
          headers: {
            'Content-Type': 'application/json'
          }
        }).
        success(function(data, status, headers, config) {
              console.log('success');
         }).
         error(function(data, status, headers, config) {
            console.log('fail');
         });
    },

    getbasketCount: function() {
      return basketCount;
    },

    updateBasketCount: function(count) {
      basketCount = count;
    }

  };

});