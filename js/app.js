'use strict';

var libraryApp = angular.module('libraryApp', ['ngRoute', 'angularUtils.directives.dirPagination'])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider.when('/search',
            {
                templateUrl:'/templates/searchLibrary.html',
                controller: 'LibraryController'
            });
        $routeProvider.when('/basket',
            {
                templateUrl:'/templates/basket.html',
                controller: 'BasketController'
            });
        $routeProvider.when('/thankyou',
            {
                templateUrl:'/templates/thankyou.html',
                controller: 'BasketController'
            });
        $routeProvider.otherwise({redirectTo: '/search'});
     //   $locationProvider.html5Mode(true);
    });
