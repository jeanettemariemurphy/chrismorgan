<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
 <?php

$data = file_get_contents("php://input");
$emailData = json_decode($data);


$firstname = $emailData->{'firstname'}; 

$lastname = $emailData->{'lastname'}; 

$email = $emailData->{'email'};

$results = $emailData->{'basket'};

$headers = "From: jeanette@meggaweb.co.uk\r\n";
$headers .= "Reply-To: jeanette@meggaweb.co.uk\r\n";
$headers .= "CC: jeanette@meggaweb.co.uk\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject = "Chris Morgans music library";

$mailto = $email;

$msg = "<body>";
$msg .= "<h2>Request received from Chris Morgans music library</h2>";
$msg .= "<p>Hello {$firstname},</p>";
$msg .= "<p>I have received your details and selections of Chris's music.  Please can you check the details below are correct and then reply to this email with your address so that I can arrange to post them to you.</p>";
$msg .= "<p>Best wishes</p>";
$msg .= "<p>Jeanette Murphy</p>";
$msg .= "<p>Name: {$firstname} {$lastname}</p>";
$msg .= "<p>Email: {$email}</p>";
$msg .= "<ul>";
foreach($results as $obj){
     $msg .= "<li>{$obj->description}</li>";
}
$msg .= "</ul>";
$msg .= "</body>";

mail($mailto, $subject,$msg,$headers);






?>
 </body>
</html>